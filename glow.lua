-- By Monsterovich
-----------------------------------
require 'cairo'
require 'cairo_xlib'

-----------------------------------
--SETTINGS

-- Glow settings

-- CPU HUNGRY!!!!!!!!!!!!!
local high_quality = true

local global_radius = 3
local global_alpha = 0.06
-- do not set samples to very high values or it will stress your cpu
local global_samples = 16

-- Other settings
local global_color1 = 0x99CCFF --0xFFB300
local global_color2 = 0xEAEAEA
local global_font1 = "GE Inspira"
local global_font2 = "Ubuntu"
local global_scale1 = 0.85
local global_scale2 = 1.0

local global_draw_network = true

-------------------------------------

function rgbToRgba(color, alpha)
    return ((color / 0x10000) % 0x100) / 255.0,((color / 0x100) % 0x100) / 255.0,(color % 0x100) / 255.0, alpha
end

function draw_text_sample(cr, text, def, ox, oy, alpha)
    local font = def.font[1]
    local fontSize = def.font[2]
    local fontSlant = CAIRO_FONT_SLANT_NORMAL
    local fontWeight = CAIRO_FONT_WEIGHT_NORMAL

    if def.font[3] and def.font[3] % 2 == 1 then
        fontWeight = CAIRO_FONT_WEIGHT_BOLD
    end
    if def.font[3] and def.font[3] / 2 >= 1 then
        fontWeight = CAIRO_FONT_SLANT_ITALIC
    end

    cairo_select_font_face(cr, font, fontSlant, fontWeight)
    cairo_set_font_size(cr, fontSize)

    cairo_set_source_rgba(cr, rgbToRgba( def.color, def.alpha*alpha))

    local x = def.pos.x+ox
    local y = def.pos.y+oy
    local te = cairo_text_extents_t:create()
    local fe = cairo_font_extents_t:create()
    cairo_text_extents(cr, text, te)
    cairo_font_extents(cr, fe)

    if def.align ~= nil then
        if def.align[1] == 'right' then
            x = x - te.width - te.x_bearing
        elseif def.align[1] == 'center' then
            x = x - te.width/2 - te.x_bearing
    end

    if def.align[2] == 'bottom' then
        y = y - fe.descent
    elseif def.align[2] == 'middle' then
        y = y + te.height/2
    elseif def.align[2] == 'top' then
        y = y + fe.ascent
    end
    end

  cairo_move_to(cr, x, y)
  cairo_show_text(cr, text)
  cairo_new_path(cr)

  local boundingBox = {
    x = x + te.x_bearing,
    y = y + te.y_bearing,
    width = te.width,
    height = te.height,
    right = x + te.x_bearing + te.width,
    bottom = y + te.y_bearing + te.height
  }

    -- free memory
    cairo_text_extents_t:destroy(te)
    cairo_font_extents_t:destroy(fe)
    return boundingBox
end

local def = {
    color = 0xffffff,
    alpha = 1.0,
    pos = {x = 0, y = 0},
    font = {'Open Sans', 48, 0},
    align = {'LEFT|right|center', 'BASELINE|bottom|middle|top'},
}

function conky_text_glow(cr, txt, x, y, color, font, font_size, font_weight, samples, radius, alpha)

    def.pos.x = x
    def.pos.y = y
    def.color = color
    def.font[1] = font
    def.font[2] = font_size
    def.font[3] = font_weight or def.font[3]
    def.alpha = alpha or def.alpha

    local samples = samples or global_samples
    local radius = radius or global_radius
    local alpha = alpha or global_alpha

    local pi_1 = 0.70710678118 -- sin(pi/4),cos(pi/4)
    local pi_2_x = 0.92387953251 -- cos(pi/8)
    local pi_2_y = 0.38268343236 -- sin(pi/8)
    local pi_3_x = pi_2_y
    local pi_3_y = pi_2_x
    local amult2 = 0.5

    if high_quality then
        radius = radius * 0.5
    end

    for i = 1, samples do
        alpha = alpha * 0.75
        draw_text_sample(cr, txt, def, radius, 0, alpha)
        draw_text_sample(cr, txt, def, 0, -radius, alpha)
        draw_text_sample(cr, txt, def, -radius, 0, alpha)
        draw_text_sample(cr, txt, def, 0, radius, alpha)

        draw_text_sample(cr, txt, def, radius*pi_1, radius*pi_1, alpha)
        draw_text_sample(cr, txt, def, radius*pi_1, -radius*pi_1, alpha)
        draw_text_sample(cr, txt, def, -radius*pi_1, -radius*pi_1, alpha)
        draw_text_sample(cr, txt, def, -radius*pi_1, radius*pi_1, alpha)

        if high_quality then
            draw_text_sample(cr, txt, def, radius*pi_2_x, radius*pi_2_y, alpha*amult2)
            draw_text_sample(cr, txt, def, radius*pi_2_x, -radius*pi_2_y, alpha*amult2)
            draw_text_sample(cr, txt, def, -radius*pi_2_x, -radius*pi_2_y, alpha*amult2)
            draw_text_sample(cr, txt, def, -radius*pi_2_x, radius*pi_2_y, alpha*amult2)

            draw_text_sample(cr, txt, def, radius*pi_3_x, radius*pi_3_y, alpha*amult2)
            draw_text_sample(cr, txt, def, radius*pi_3_x, -radius*pi_3_y, alpha*amult2)
            draw_text_sample(cr, txt, def, -radius*pi_3_x, -radius*pi_3_y, alpha*amult2)
            draw_text_sample(cr, txt, def, -radius*pi_3_x, radius*pi_3_y, alpha*amult2)
        end
        radius = radius * 1.25
    end

    return draw_text_sample(cr, txt, def, 0, 0, 1.0)
end

function conky_bar(cr, p, x, y)
    cairo_move_to (cr, x, y);
    cairo_line_to (cr, x+(p/100)*50, y);
end

function percent_to_color(p)
    if p <= 25 then
        return 0x00B800
    elseif p <= 50 then
        return 0xFFFF00
    elseif p <= 75 then
        return 0xFF9000
    elseif p <= 100 then
        return 0xFF2600
    end
end

-- Draw everything
function conky_draw(x, y)
    if conky_window==nil then return end
    local w=conky_window.width
    local h=conky_window.height
    local cs=cairo_xlib_surface_create(conky_window.display, conky_window.drawable, conky_window.visual, w, h)
    local cr=cairo_create(cs)

    local color1 = global_color1
    local color2 = global_color2
    local font2 = global_font1
    local font1 = global_font2
    local scale = global_scale1
    local scale2 = global_scale2

    local cur = conky_text_glow(cr, conky_parse("${time %k:%M}"), x, y, color2, font2, 120*scale)
    local cur2 = cur
    cur = conky_text_glow(cr, conky_parse("${time %d}"), x+cur.width+40, y-cur.height+30, color1, font2, 42*scale)
    cur = conky_text_glow(cr, conky_parse("${time  %B} ${time %Y}"), cur.x+cur.width+10, cur.y-5+cur.height/9*10, color2, font2, 22*scale)

    conky_text_glow(cr, conky_parse("${time %A}"), x+cur2.width+50, y-cur2.height+80, color2, font2, 42*scale)


    local line_y = y+40
    x = x
    cur = conky_text_glow(cr, "HD", x+25, line_y, color1, font1, 12*scale2)
    cur = conky_text_glow(cr, conky_parse("${fs_free /} / ${fs_size /}"), cur.x+cur.width+10, cur.y+cur.height, color2, font1, 12*scale2)

    local gap = 150
    cur = conky_text_glow(cr, "RAM", cur.x+gap, line_y, color1, font1, 12*scale2)
    cur = conky_text_glow(cr, conky_parse("$mem / $memmax"), cur.x+cur.width+10, cur.y+cur.height, color2, font1, 12*scale2)

    cur = conky_text_glow(cr, "SWAP", x+25, line_y+20, color1, font1, 12*scale2)
    cur = conky_text_glow(cr, conky_parse("$swap / $swapmax"), cur.x+cur.width+10, cur.y+cur.height, color2, font1, 12*scale2)

    -- all cpus combined
    cur = conky_text_glow(cr, "CPU", cur.x+gap-16, line_y+20, color1, font1, 12*scale2)
    cur = conky_text_glow(cr, conky_parse("${cpu cpu0}%"), cur.x+cur.width+10, cur.y+cur.height, color2, font1, 12*scale2)

    line_y = y + 100
    gap = 22

    x = x + 10

    cur = conky_text_glow(cr, "PROCESSORS ____________________________________", x+1020, line_y-145, color2, font1, 12*scale2)
    local cur3 = cur

    local cpu1 = conky_text_glow(cr, "CPU1: ", cur.x-1, cur.y+30, color1, font1, 12*scale2)
    local cpu2 = conky_text_glow(cr, "CPU2: ", cpu1.x, cpu1.y+gap, color1, font1, 12*scale2)
    local cpu3 = conky_text_glow(cr, "CPU3: ", cpu2.x, cpu2.y+gap, color1, font1, 12*scale2)
    local cpu4 = conky_text_glow(cr, "CPU4: ", cpu3.x, cpu3.y+gap, color1, font1, 12*scale2)

    local tcol = percent_to_color(tonumber(conky_parse("${cpu cpu1}")))
    cur = conky_text_glow(cr, conky_parse("${cpu cpu1}%"), cpu1.x+cpu1.width+5, cpu1.y+cpu1.height, tcol, font1, 12*scale2)
    tcol = percent_to_color(tonumber(conky_parse("${cpu cpu2}")))
    cur = conky_text_glow(cr, conky_parse("${cpu cpu2}%"), cpu2.x+cpu1.width+5, cpu2.y+cpu2.height, tcol, font1, 12*scale2)
    tcol = percent_to_color(tonumber(conky_parse("${cpu cpu3}")))
    cur = conky_text_glow(cr, conky_parse("${cpu cpu3}%"), cpu3.x+cpu1.width+5, cpu3.y+cpu3.height, tcol, font1, 12*scale2)
    tcol = percent_to_color(tonumber(conky_parse("${cpu cpu4}")))
    cur = conky_text_glow(cr, conky_parse("${cpu cpu4}%"), cpu4.x+cpu1.width+5, cpu4.y+cpu4.height, tcol, font1, 12*scale2)
    -- reset color
    cairo_set_source_rgba(cr, rgbToRgba( 0xFFFFFF, 1.0))

    local xoff = -60
    cairo_set_line_width (cr, 5.0*scale2);
    cairo_set_line_cap (cr, CAIRO_LINE_CAP_ROUND);
    local add_gap = 0
    conky_bar(cr, tonumber(conky_parse("${cpu cpu1}")), cur3.right+xoff, cur3.y+gap+add_gap)
    add_gap = add_gap + 14
    conky_bar(cr, tonumber(conky_parse("${cpu cpu2}")), cur3.right+xoff, cur3.y+gap+add_gap)
    add_gap = add_gap + 14
    conky_bar(cr, tonumber(conky_parse("${cpu cpu3}")), cur3.right+xoff, cur3.y+gap+add_gap)
    add_gap = add_gap + 14
    conky_bar(cr, tonumber(conky_parse("${cpu cpu4}")), cur3.right+xoff, cur3.y+gap+add_gap)
    cairo_stroke (cr);

    xoff = -160
    cur = conky_text_glow(cr, conky_parse("${exec awk '/cpu MHz/{i++}i==1{printf \"%.f\",$4; exit}' /proc/cpuinfo} MHz"), cur3.right+xoff, cur3.y+cur3.height/2+gap, color2, font1, 12*scale2)
    cur = conky_text_glow(cr, conky_parse("${exec awk '/cpu MHz/{i++}i==2{printf \"%.f\",$4; exit}' /proc/cpuinfo} MHz"), cur3.right+xoff, cur.y+gap, color2, font1, 12*scale2)
    cur = conky_text_glow(cr, conky_parse("${exec awk '/cpu MHz/{i++}i==3{printf \"%.f\",$4; exit}' /proc/cpuinfo} MHz"), cur3.right+xoff, cur.y+gap, color2, font1, 12*scale2)
    cur = conky_text_glow(cr, conky_parse("${exec awk '/cpu MHz/{i++}i==4{printf \"%.f\",$4; exit}' /proc/cpuinfo} MHz"), cur3.right+xoff, cur.y+gap, color2, font1, 12*scale2)

    if global_draw_network then
        cur = conky_text_glow(cr, "NETWORK _______________________________________", cpu1.x, cur.y+gap*2, color2, font1, 12*scale2)
        cur = conky_text_glow(cr, "IP: ", cur.x, cur.y+gap+3, color1, font1, 12*scale2)
        local ip = conky_parse("${curl icanhazip.com}");
        cur = conky_text_glow(cr, ip:sub(1, -2), cur.right-xoff-1, cur.bottom, color2, font1, 12*scale2)
    end

    cairo_surface_destroy(cs)
    cairo_destroy(cr)
    collectgarbage()
    return ""
end
